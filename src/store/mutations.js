export default{
	changeCity(state,city){//state 是全局的state数据 city是commit传进来的参数
		state.city = city //state 里city的数据 等于 组件传进来的city
		try{
			localStorage.city = city //localStorage本地存储
		}catch(e){}
	}
}