import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'
import getters from './getter'

Vue.use(Vuex)//vue中调用插件 使用 Vue.use()方法 

// let defaultCity = "郑州"
// try{
// 	if(localStorage.city){    //try catch这种写法是为了以防用户浏览器使用了隐身模式或者浏览器不兼容
// 		defaultCity = localStorage.city
// 	}
// } catch(e){}

export default new Vuex.Store({
	state:state,  //state数据存储在state.js中
	actions:{  //dispatch 传进来的函数要在 actions中调用
		changeCity(ctx, city){    
			ctx.commit('changeCity',city) //actions通过commit向mutations通信传参  
										  //第一个参数是要传到mutations里的方法 第二个是需要的城市
		}
	},
	mutations:mutations,   //mutations 存放在mutations.js中
	getters:getters //vuex 中的getters 好比 vue中的computed计算属性 
})