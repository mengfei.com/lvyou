let defaultCity = "郑州"
try{
	if(localStorage.city){    //try catch这种写法是为了以防用户浏览器使用了隐身模式或者浏览器不兼容
		defaultCity = localStorage.city
	}
} catch(e){}

export default{
	city:defaultCity  //全局数据
}